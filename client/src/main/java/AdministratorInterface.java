import com.epam.webservices.soap.Book;
import com.epam.webservices.soap.ISoapService;
import com.epam.webservices.soap.User;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Scanner;

public class AdministratorInterface extends UserInterface implements ISystemUserInterface {

	private static Logger logger = Logger.getLogger(AdministratorInterface.class);
	private static Scanner scanner = new Scanner(System.in);

	AdministratorInterface(ISoapService port) {
		super(port);
	}

	@Override
	public void printCommandList() {
		super.printCommandList();
		logger.info(localization.getValue("client.interface.admin.command.list"));
	}

	@Override
	public void doCommand(String input) {
		switch (input) {
			case "6":
				if (addBook()) {
					logger.info(localization.getValue("book.add.success"));
				} else {
					logger.info(localization.getValue("book.add.failure"));
				}
				break;
			case "7":
				if (deleteBook()) {
					logger.info(localization.getValue("book.delete.success"));
				} else {
					logger.info(localization.getValue("book.delete.failure"));
				}
				break;
			case "8":
				if (setRole()) {
					logger.info(localization.getValue("role.set.success"));
				} else {
					logger.info(localization.getValue("role.set.failure"));
				}
				break;
			case "9":
				if (addUser()) {
					logger.info(localization.getValue("user.add.success"));
				} else {
					logger.info(localization.getValue("user.add.failure"));
				}
				break;
			case "10":
				List<User> users = getUsers();
				if (users.size() > 0) {
					users.forEach(user -> logger.info(user.toString()));
				} else {
					logger.info(localization.getValue("user.get.not.found"));
				}
				break;
			default:
				super.doCommand(input);
		}
	}


	private boolean addBook() {
		Book book = new Book();
		book.setISBN(CheckInput.checkInt(localization.getValue("book.input.isbn")));
		book.setAUTHOR(CheckInput.checkString(localization.getValue("book.input.author.name")));
		book.setDATE(new com.epam.webservices.soap.Date());
		book.setNAME(CheckInput.checkString(localization.getValue("book.input.name")));
		return port.addBook(book);
	}

	private boolean deleteBook() {
		long isbn = CheckInput.checkInt(localization.getValue("book.input.isbn"));
		return port.deleteBook(isbn);
	}

	private boolean setRole() {
		User user = new User();
		user.setID(CheckInput.checkInt(localization.getValue("user.input.id")));
		return port.changeUserRole(user,
				CheckInput.getRole(localization.getValue("role.input.role"),
						localization.getValue("role.list")));
	}

	private boolean addUser() {
		User user = new User();
		user.setNAME(CheckInput.checkString(localization.getValue("user.input.name")));
		user.setPASSWORDHASH(CheckInput.checkString(localization.getValue("user.input.password")));
		user.setROLE(CheckInput.getRole(localization.getValue("role.input.role"), localization.getValue("role.list")));
		return port.addUser(user);
	}

	private List<User> getUsers() {
		String name = CheckInput.checkString(localization.getValue("user.input.name"));
		return port.getUsers(name);
	}

}
