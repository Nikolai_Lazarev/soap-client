import com.epam.webservices.soap.Role;
import org.apache.log4j.Logger;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class CheckInput {

	protected static Scanner scanner = new Scanner(System.in);
	private static Logger logger = Logger.getLogger(CheckInput.class);


	public static long getTimeFromDate(SimpleDateFormat format){
		long time = 0;
		try{
			time = format.parse(String.valueOf(checkInt("input year"))).getTime();
		}catch (ParseException e){
			logger.error(e);
		}
		return time;
	}

	public static int checkInt(String message){
		String input;
		logger.info(message);
		while(!(input = scanner.nextLine()).matches("\\d+")){
			logger.info(message);
		}
		return Integer.valueOf(input);
	}

	public static String checkString(String message){
		String input;
		logger.info(message);
		input = scanner.nextLine();
		return input;
	}

	public static Role getRole(String message, String rolesList){
		logger.info(message);
		logger.info(rolesList);
		String input;

		while(!(input = scanner.nextLine()).matches("\\d+")){
			logger.info(message);
		}
		switch (input){
			case "1":
				return Role.ADMIN;
			case "2":
				return Role.BASE;
			default:
				return Role.BASE;
		}
	}
}
