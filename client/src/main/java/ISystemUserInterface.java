import java.util.Scanner;

public interface ISystemUserInterface {
	LocalizationManager localization = LocalizationManager.getInstance();
	Scanner scanner = new Scanner(System.in);
	void printCommandList();
	void doCommand(String input);
}
