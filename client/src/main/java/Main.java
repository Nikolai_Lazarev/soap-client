import com.epam.webservices.soap.ISoapService;
import com.epam.webservices.soap.Role;
import com.epam.webservices.soap.SoapServiceService;
import com.epam.webservices.soap.User;

import javax.xml.ws.BindingProvider;
import java.util.Scanner;

public class Main {


	public static void main(String[] args) {
		SoapServiceService soapServiceService = new SoapServiceService();
		ISoapService port = soapServiceService.getSoapServicePort();
		BindingProvider prov = (BindingProvider) port;
		Scanner scanner = new Scanner(System.in);
		prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "Supeser");
		prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "123123");
		User user = port.getAuthenticatedUser();
		ISystemUserInterface userInterface = null;
		if (user != null) {
			if (user.getROLE() == Role.ADMIN) {
				userInterface = new AdministratorInterface(port);
			} else if (user.getROLE() == Role.BASE)
				userInterface = new UserInterface(port);
		}
		if (userInterface != null) {
			userInterface.printCommandList();
			String input;
			while (!"exit".equalsIgnoreCase((input = scanner.nextLine()))) {
				//userInterface.printCommandList();
				userInterface.doCommand(input);
			}
		}
	}
}
