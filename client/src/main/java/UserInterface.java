import com.epam.webservices.soap.Author;
import com.epam.webservices.soap.Book;
import com.epam.webservices.soap.Bookmark;
import com.epam.webservices.soap.ISoapService;
import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import org.apache.log4j.Logger;

import java.util.List;


public class UserInterface implements ISystemUserInterface {

	private static Logger logger = Logger.getLogger(UserInterface.class);
	protected ISoapService port;

	UserInterface(ISoapService port) {
		this.port = port;
	}

	@Override
	public void printCommandList() {
		logger.info(localization.getValue("client.interface.user.command.list"));
	}

	@Override
	public void doCommand(String input) {
		switch (input) {
			case "1":
				if (addBookmark()) {
					logger.info(localization.getValue("bookmark.add.success"));
				} else {
					logger.info(localization.getValue("bookmark.add.failure"));
				}
				break;
			case "2":
				findBooks().forEach(book -> logger.info(book.toString()));
				break;
			case "3":
				showAllBookmark().forEach(bookmark -> logger.info(bookmark.toString()));
				break;
			case "4":
				if (deleteBookmark()) {
					logger.info(localization.getValue("bookmark.delete.success"));
				}
				break;
			case "5":
				getAuthors().forEach(author -> logger.info(author.toString()));
				break;
			default:
		}

	}

	protected boolean addBookmark() {
		int isbn = CheckInput.checkInt(localization.getValue("book.input.isbn"));
		int page = CheckInput.checkInt(localization.getValue("bookmark.input.page"));
		Book book = new Book();
		book.setISBN(isbn);
		return port.addBookmark(book, page);
	}

	private List<Book> findBookByISBN(int isbn){
		List <Book> books = port.getBooks(isbn, null, 0, 0, 0);
		return books;
	}

	protected List<Book> findBooks() {
		int isbn = CheckInput.checkInt(localization.getValue("book.input.isbn"));
		if(isbn!=0){
			return findBookByISBN(isbn);
		}
		String name = CheckInput.checkString(localization.getValue("book.input.name"));
		int authorId = CheckInput.checkInt(localization.getValue("book.input.author.id"));
		long startTime = CheckInput.checkInt(localization.getValue("book.input.time.start"));
		long endTime = CheckInput.checkInt(localization.getValue("book.input.time.end"));
		Book book = new Book();
		return port.getBooks(isbn, name, authorId, startTime, endTime);
	}

	protected List<Bookmark> showAllBookmark() {
		return port.getBookmarks();
	}

	protected boolean deleteBookmark() {
		try {
			int id = CheckInput.checkInt(localization.getValue("bookmark.input.id"));
			return port.deleteBookmark(id);
		}catch (ServerSOAPFaultException e){
			logger.error(e.getMessage());
			return false;
		}
	}

	protected List<Author> getAuthors() {
		return port.getAuthors();
	}

}
