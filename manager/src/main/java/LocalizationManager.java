import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

public class LocalizationManager {
	private Logger logger = Logger.getLogger(LocalizationManager.class);
	private Properties properties;
	private static LocalizationManager instance;


	private LocalizationManager(){
		try {
			properties = new Properties();
			properties.load(getClass().getClassLoader().getResourceAsStream("English.properties"));
		}catch (IOException e){
			logger.error(e);
		}
	}

	public static LocalizationManager getInstance(){
		if(instance == null){
			instance = new LocalizationManager();
		}
		return instance;
	}

	public String getValue(String key){
		return properties.getProperty(key);
	}
}
