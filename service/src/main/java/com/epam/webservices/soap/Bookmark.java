
package com.epam.webservices.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for bookmark complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bookmark">
 *   &lt;complexContent>
 *     &lt;extension base="{http://soap.webservices.epam.com/}entity">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BOOK" type="{http://soap.webservices.epam.com/}book" minOccurs="0"/>
 *         &lt;element name="PAGE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bookmark", propOrder = {
    "id",
    "book",
    "page"
})
public class Bookmark
    extends Entity
{

    @XmlElement(name = "ID")
    protected int id;
    @XmlElement(name = "BOOK")
    protected Book book;
    @XmlElement(name = "PAGE")
    protected int page;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setID(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the book property.
     * 
     * @return
     *     possible object is
     *     {@link Book }
     *     
     */
    public Book getBOOK() {
        return book;
    }

    /**
     * Sets the value of the book property.
     * 
     * @param value
     *     allowed object is
     *     {@link Book }
     *     
     */
    public void setBOOK(Book value) {
        this.book = value;
    }

    /**
     * Gets the value of the page property.
     * 
     */
    public int getPAGE() {
        return page;
    }

    /**
     * Sets the value of the page property.
     * 
     */
    public void setPAGE(int value) {
        this.page = value;
    }

}
