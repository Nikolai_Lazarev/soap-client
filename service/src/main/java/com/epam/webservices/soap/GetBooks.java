
package com.epam.webservices.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBooks complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBooks">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="isbn" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="authorId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="firstTimestamp" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="secondTimestamp" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBooks", propOrder = {
    "isbn",
    "name",
    "authorId",
    "firstTimestamp",
    "secondTimestamp"
})
public class GetBooks {

    protected long isbn;
    protected String name;
    protected int authorId;
    protected long firstTimestamp;
    protected long secondTimestamp;

    /**
     * Gets the value of the isbn property.
     * 
     */
    public long getIsbn() {
        return isbn;
    }

    /**
     * Sets the value of the isbn property.
     * 
     */
    public void setIsbn(long value) {
        this.isbn = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the authorId property.
     * 
     */
    public int getAuthorId() {
        return authorId;
    }

    /**
     * Sets the value of the authorId property.
     * 
     */
    public void setAuthorId(int value) {
        this.authorId = value;
    }

    /**
     * Gets the value of the firstTimestamp property.
     * 
     */
    public long getFirstTimestamp() {
        return firstTimestamp;
    }

    /**
     * Sets the value of the firstTimestamp property.
     * 
     */
    public void setFirstTimestamp(long value) {
        this.firstTimestamp = value;
    }

    /**
     * Gets the value of the secondTimestamp property.
     * 
     */
    public long getSecondTimestamp() {
        return secondTimestamp;
    }

    /**
     * Sets the value of the secondTimestamp property.
     * 
     */
    public void setSecondTimestamp(long value) {
        this.secondTimestamp = value;
    }

}
