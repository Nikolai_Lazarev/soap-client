
package com.epam.webservices.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.epam.webservices.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetBooksResponse_QNAME = new QName("http://soap.webservices.epam.com/", "getBooksResponse");
    private final static QName _AddBook_QNAME = new QName("http://soap.webservices.epam.com/", "addBook");
    private final static QName _GetBookmarksResponse_QNAME = new QName("http://soap.webservices.epam.com/", "getBookmarksResponse");
    private final static QName _GetAuthorsResponse_QNAME = new QName("http://soap.webservices.epam.com/", "getAuthorsResponse");
    private final static QName _DeleteBookmarkResponse_QNAME = new QName("http://soap.webservices.epam.com/", "deleteBookmarkResponse");
    private final static QName _DeleteBookmark_QNAME = new QName("http://soap.webservices.epam.com/", "deleteBookmark");
    private final static QName _AddUser_QNAME = new QName("http://soap.webservices.epam.com/", "addUser");
    private final static QName _ChangeUserRole_QNAME = new QName("http://soap.webservices.epam.com/", "changeUserRole");
    private final static QName _AddBookResponse_QNAME = new QName("http://soap.webservices.epam.com/", "addBookResponse");
    private final static QName _AddBookmarkResponse_QNAME = new QName("http://soap.webservices.epam.com/", "addBookmarkResponse");
    private final static QName _GetAuthenticatedUser_QNAME = new QName("http://soap.webservices.epam.com/", "getAuthenticatedUser");
    private final static QName _AddBookmark_QNAME = new QName("http://soap.webservices.epam.com/", "addBookmark");
    private final static QName _ChangeUserRoleResponse_QNAME = new QName("http://soap.webservices.epam.com/", "changeUserRoleResponse");
    private final static QName _GetBooks_QNAME = new QName("http://soap.webservices.epam.com/", "getBooks");
    private final static QName _GetUsersResponse_QNAME = new QName("http://soap.webservices.epam.com/", "getUsersResponse");
    private final static QName _GetBookmarks_QNAME = new QName("http://soap.webservices.epam.com/", "getBookmarks");
    private final static QName _AddUserResponse_QNAME = new QName("http://soap.webservices.epam.com/", "addUserResponse");
    private final static QName _GetAuthors_QNAME = new QName("http://soap.webservices.epam.com/", "getAuthors");
    private final static QName _GetUsers_QNAME = new QName("http://soap.webservices.epam.com/", "getUsers");
    private final static QName _GetAuthenticatedUserResponse_QNAME = new QName("http://soap.webservices.epam.com/", "getAuthenticatedUserResponse");
    private final static QName _DeleteBookResponse_QNAME = new QName("http://soap.webservices.epam.com/", "deleteBookResponse");
    private final static QName _DeleteBook_QNAME = new QName("http://soap.webservices.epam.com/", "deleteBook");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.epam.webservices.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddBookmark }
     * 
     */
    public AddBookmark createAddBookmark() {
        return new AddBookmark();
    }

    /**
     * Create an instance of {@link ChangeUserRoleResponse }
     * 
     */
    public ChangeUserRoleResponse createChangeUserRoleResponse() {
        return new ChangeUserRoleResponse();
    }

    /**
     * Create an instance of {@link GetBooks }
     * 
     */
    public GetBooks createGetBooks() {
        return new GetBooks();
    }

    /**
     * Create an instance of {@link GetAuthenticatedUser }
     * 
     */
    public GetAuthenticatedUser createGetAuthenticatedUser() {
        return new GetAuthenticatedUser();
    }

    /**
     * Create an instance of {@link AddBookResponse }
     * 
     */
    public AddBookResponse createAddBookResponse() {
        return new AddBookResponse();
    }

    /**
     * Create an instance of {@link AddBookmarkResponse }
     * 
     */
    public AddBookmarkResponse createAddBookmarkResponse() {
        return new AddBookmarkResponse();
    }

    /**
     * Create an instance of {@link AddUser }
     * 
     */
    public AddUser createAddUser() {
        return new AddUser();
    }

    /**
     * Create an instance of {@link ChangeUserRole }
     * 
     */
    public ChangeUserRole createChangeUserRole() {
        return new ChangeUserRole();
    }

    /**
     * Create an instance of {@link DeleteBookmark }
     * 
     */
    public DeleteBookmark createDeleteBookmark() {
        return new DeleteBookmark();
    }

    /**
     * Create an instance of {@link DeleteBookmarkResponse }
     * 
     */
    public DeleteBookmarkResponse createDeleteBookmarkResponse() {
        return new DeleteBookmarkResponse();
    }

    /**
     * Create an instance of {@link GetAuthorsResponse }
     * 
     */
    public GetAuthorsResponse createGetAuthorsResponse() {
        return new GetAuthorsResponse();
    }

    /**
     * Create an instance of {@link AddBook }
     * 
     */
    public AddBook createAddBook() {
        return new AddBook();
    }

    /**
     * Create an instance of {@link GetBookmarksResponse }
     * 
     */
    public GetBookmarksResponse createGetBookmarksResponse() {
        return new GetBookmarksResponse();
    }

    /**
     * Create an instance of {@link GetBooksResponse }
     * 
     */
    public GetBooksResponse createGetBooksResponse() {
        return new GetBooksResponse();
    }

    /**
     * Create an instance of {@link DeleteBook }
     * 
     */
    public DeleteBook createDeleteBook() {
        return new DeleteBook();
    }

    /**
     * Create an instance of {@link DeleteBookResponse }
     * 
     */
    public DeleteBookResponse createDeleteBookResponse() {
        return new DeleteBookResponse();
    }

    /**
     * Create an instance of {@link GetAuthenticatedUserResponse }
     * 
     */
    public GetAuthenticatedUserResponse createGetAuthenticatedUserResponse() {
        return new GetAuthenticatedUserResponse();
    }

    /**
     * Create an instance of {@link GetAuthors }
     * 
     */
    public GetAuthors createGetAuthors() {
        return new GetAuthors();
    }

    /**
     * Create an instance of {@link GetUsers }
     * 
     */
    public GetUsers createGetUsers() {
        return new GetUsers();
    }

    /**
     * Create an instance of {@link AddUserResponse }
     * 
     */
    public AddUserResponse createAddUserResponse() {
        return new AddUserResponse();
    }

    /**
     * Create an instance of {@link GetBookmarks }
     * 
     */
    public GetBookmarks createGetBookmarks() {
        return new GetBookmarks();
    }

    /**
     * Create an instance of {@link GetUsersResponse }
     * 
     */
    public GetUsersResponse createGetUsersResponse() {
        return new GetUsersResponse();
    }

    /**
     * Create an instance of {@link Date }
     * 
     */
    public Date createDate() {
        return new Date();
    }

    /**
     * Create an instance of {@link Book }
     * 
     */
    public Book createBook() {
        return new Book();
    }

    /**
     * Create an instance of {@link Author }
     * 
     */
    public Author createAuthor() {
        return new Author();
    }

    /**
     * Create an instance of {@link Bookmark }
     * 
     */
    public Bookmark createBookmark() {
        return new Bookmark();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBooksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "getBooksResponse")
    public JAXBElement<GetBooksResponse> createGetBooksResponse(GetBooksResponse value) {
        return new JAXBElement<GetBooksResponse>(_GetBooksResponse_QNAME, GetBooksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddBook }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "addBook")
    public JAXBElement<AddBook> createAddBook(AddBook value) {
        return new JAXBElement<AddBook>(_AddBook_QNAME, AddBook.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBookmarksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "getBookmarksResponse")
    public JAXBElement<GetBookmarksResponse> createGetBookmarksResponse(GetBookmarksResponse value) {
        return new JAXBElement<GetBookmarksResponse>(_GetBookmarksResponse_QNAME, GetBookmarksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAuthorsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "getAuthorsResponse")
    public JAXBElement<GetAuthorsResponse> createGetAuthorsResponse(GetAuthorsResponse value) {
        return new JAXBElement<GetAuthorsResponse>(_GetAuthorsResponse_QNAME, GetAuthorsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteBookmarkResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "deleteBookmarkResponse")
    public JAXBElement<DeleteBookmarkResponse> createDeleteBookmarkResponse(DeleteBookmarkResponse value) {
        return new JAXBElement<DeleteBookmarkResponse>(_DeleteBookmarkResponse_QNAME, DeleteBookmarkResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteBookmark }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "deleteBookmark")
    public JAXBElement<DeleteBookmark> createDeleteBookmark(DeleteBookmark value) {
        return new JAXBElement<DeleteBookmark>(_DeleteBookmark_QNAME, DeleteBookmark.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "addUser")
    public JAXBElement<AddUser> createAddUser(AddUser value) {
        return new JAXBElement<AddUser>(_AddUser_QNAME, AddUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeUserRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "changeUserRole")
    public JAXBElement<ChangeUserRole> createChangeUserRole(ChangeUserRole value) {
        return new JAXBElement<ChangeUserRole>(_ChangeUserRole_QNAME, ChangeUserRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddBookResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "addBookResponse")
    public JAXBElement<AddBookResponse> createAddBookResponse(AddBookResponse value) {
        return new JAXBElement<AddBookResponse>(_AddBookResponse_QNAME, AddBookResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddBookmarkResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "addBookmarkResponse")
    public JAXBElement<AddBookmarkResponse> createAddBookmarkResponse(AddBookmarkResponse value) {
        return new JAXBElement<AddBookmarkResponse>(_AddBookmarkResponse_QNAME, AddBookmarkResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAuthenticatedUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "getAuthenticatedUser")
    public JAXBElement<GetAuthenticatedUser> createGetAuthenticatedUser(GetAuthenticatedUser value) {
        return new JAXBElement<GetAuthenticatedUser>(_GetAuthenticatedUser_QNAME, GetAuthenticatedUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddBookmark }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "addBookmark")
    public JAXBElement<AddBookmark> createAddBookmark(AddBookmark value) {
        return new JAXBElement<AddBookmark>(_AddBookmark_QNAME, AddBookmark.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeUserRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "changeUserRoleResponse")
    public JAXBElement<ChangeUserRoleResponse> createChangeUserRoleResponse(ChangeUserRoleResponse value) {
        return new JAXBElement<ChangeUserRoleResponse>(_ChangeUserRoleResponse_QNAME, ChangeUserRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBooks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "getBooks")
    public JAXBElement<GetBooks> createGetBooks(GetBooks value) {
        return new JAXBElement<GetBooks>(_GetBooks_QNAME, GetBooks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "getUsersResponse")
    public JAXBElement<GetUsersResponse> createGetUsersResponse(GetUsersResponse value) {
        return new JAXBElement<GetUsersResponse>(_GetUsersResponse_QNAME, GetUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBookmarks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "getBookmarks")
    public JAXBElement<GetBookmarks> createGetBookmarks(GetBookmarks value) {
        return new JAXBElement<GetBookmarks>(_GetBookmarks_QNAME, GetBookmarks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "addUserResponse")
    public JAXBElement<AddUserResponse> createAddUserResponse(AddUserResponse value) {
        return new JAXBElement<AddUserResponse>(_AddUserResponse_QNAME, AddUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAuthors }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "getAuthors")
    public JAXBElement<GetAuthors> createGetAuthors(GetAuthors value) {
        return new JAXBElement<GetAuthors>(_GetAuthors_QNAME, GetAuthors.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "getUsers")
    public JAXBElement<GetUsers> createGetUsers(GetUsers value) {
        return new JAXBElement<GetUsers>(_GetUsers_QNAME, GetUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAuthenticatedUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "getAuthenticatedUserResponse")
    public JAXBElement<GetAuthenticatedUserResponse> createGetAuthenticatedUserResponse(GetAuthenticatedUserResponse value) {
        return new JAXBElement<GetAuthenticatedUserResponse>(_GetAuthenticatedUserResponse_QNAME, GetAuthenticatedUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteBookResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "deleteBookResponse")
    public JAXBElement<DeleteBookResponse> createDeleteBookResponse(DeleteBookResponse value) {
        return new JAXBElement<DeleteBookResponse>(_DeleteBookResponse_QNAME, DeleteBookResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteBook }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.webservices.epam.com/", name = "deleteBook")
    public JAXBElement<DeleteBook> createDeleteBook(DeleteBook value) {
        return new JAXBElement<DeleteBook>(_DeleteBook_QNAME, DeleteBook.class, null, value);
    }

}
